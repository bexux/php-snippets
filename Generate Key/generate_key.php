<?php

/*Generate Key

Create an API to generate a key code (serial number) SiteA will make a call to the SiteB to validate, and ask for a serial number to be generated.

1. Request Key
	-pass email and format (JSON, txt or XML)
2. Generate Key 
	-create a file with email
	-email_at_domain_DOTcom
	-content fill
	-Return key to API
3. Send Key-output in 3 formats, JSON, txt, and XML

*/

/*
This function will check if all the params were passed in the url. Condition: return true or false. 
*/

function getFileName(){
	$fileName = str_replace('@', '_AT_', $_REQUEST['email']);
	$fileName = str_replace('.', '_DOT_', $fileName);
	return $fileName;
}

function processData(){
	$return = false; //always assume user is wrong
	//validate param (email and format)
	//use $_REQUEST	if you don't know if they used POST or GET
	//check if the params exist
	if(!isset($_REQUEST['email'])){
		return false;
	}
	$return = filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL); //returns true or false

	checkFormat();
	return $return;
}
/*Passing an error message dynamically*/
function error($message){
	//display in the format the user chose
	switch($_REQUEST['fileFormat']){
		case 'xml':
			//xml message
			break;
		case 'json':
			//json message
			break;
		default: 
			return $message;
	}
}

function genKey(){
	$key = md5($_REQUEST['email']);
	return $key;
}

function checkFormat(){
	if(!isset($_REQUEST['fileFormat'])){
		$_REQUEST['fileFormat'] = 'txt';
	}
	switch($_REQUEST['fileFormat']){
		case 'txt':
		case 'xml':
		case 'json':
			return true;
		default: 
			$_REQUEST['fileFormat'] = 'txt';
	}
}

function save($key){
	//saves key to a file and displays to user
	switch($_REQUEST['fileFormat']){
		case 'xml':
			// $content = xml message
			break;
		case 'json':
			// $content = json message
			break;
		default: 
			$content = $key;
	}
	$save = file_put_contents('C://' . getFileName(), $content);
	return (bool)$save; //will return true or false because it is converted to boolean (Num > 0 always true)
}

//show to the user
function display(){
	//Just read the file and display what is in it
	//automatically has the format taken care of in $content
	echo file_get_contents('C://' . getFileName());
}


if(processData() == true){
	$key = genKey();
	if(save($key) == true){
		display();
	}
	else {
		$message = 'Error Code 2';
		echo error($message);
	}
}
else{
	$message = 'Error Code 1: Invalid Data Provided';
	echo error($message);
}