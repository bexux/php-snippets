<?php

class MyDB extends PDO {

    private static $instance = NULL;

    public function __construct() {
       try{
            parent::__construct('mysql:host=localhost;dbname=food','root','');
        }
        catch(PDOException $e){
            die($e->getMessage()); //if we have an error, kill application and output an error message
        }
    }

    public function getData(){

        //$sql = 'SELECT ' . $field . ' FROM sortable';
        $sql = 'SELECT * FROM sortable.sortable';
        
        $sth = $this->prepare($sql);

        $code = $sth->execute();
       
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function putData($id, $pos) {
        $sql = 'INSERT IGNORE INTO sortable' . ' SET pos =' . $pos . ' WHERE id = ' . $id; 
        $sth = $this->prepare($sql);

        $code = $sth->execute();
    }
}



