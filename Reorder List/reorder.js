$(function() {
		
		$( "#sortable" ).sortable({
			stop: function( event, ui ) {
				//var list = $('#sortable').find('li.item').toArray();
				var a = [];
				$('#sortable').find('li.item').each(function(e){
					if($(this).hasClass('ui-sortable-placeholder')){
						return;
					}
					a.push( $(this).data('id') );
				});
				var list = a.join( "," );
				$.ajax({        
			       type: "POST",
			       url: "reorder.php",
			       data: {list : list},
			       success: function(data){
			       	console.log(data);
			       }
			    }); 
			}
		});
		$( "#sortable" ).disableSelection();
		});