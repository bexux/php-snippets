<?php
//set up database connection
require('Database.class.php');

$DB = new MyDB();

$DB->getData();

//Get the list of names from the database
$sql = 'SELECT * FROM sortable.sortable ORDER BY pos';
$sth = $DB->prepare($sql);

$code = $sth->execute();

$data = $sth->fetchAll(PDO::FETCH_ASSOC);


//style the page and add in the sortablejs files
$header='
<head>
	<meta charset="UTF-8">
	<title>Reorder List PHP Tut</title>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="reorder.js"></script>
	<link rel="stylesheet" href="reorder.css">
</head>';

//populate the li's with data from the database
$list = '';

foreach($data as $dat){
	$list .= '<li class="item" data-id="' . $dat['id'] . '"><span>' . $dat['name'] . '</span></li>';
}

//display the li's
echo $header;
echo '<div id="wrapper"><ol id="sortable">' . $list . '</ol>';

//When the names are dragged and reordered, ajax will call reorder.php

?>

<a href="random.php" class="random">Randomize</a>
</div>