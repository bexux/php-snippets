<?php

//Write a program to validate a credit card #. Need a from with an input for the # and a drop down for credit card type

//Validate if the string starts with a certain first number and if it matches the drop down option
// 4 for Visa, 5 for MC, 2 for discover
$cc = $_POST['ccNum'];
$firstNum = substr($cc, 0, 1);
$type = $_POST['cardType'];
 
function ccType($i){
	if($i == 2 || $i == 4 || $i == 5){
		return true;
	}
	else{
		return false;
	}
}
function matchType($i, $t){
	if($i == 2 && $t == 3){
		return true;
	}
	else if($i == 4 && $t == 1){
		return true;
	}
	else if($i == 5 && $t == 2){
		return true;
	}
	else{
		return false;
	}
}
function validate($i,$t){
	//$i being the full cc number
	//$t being type 1, 2 or 3
	$lastNum = substr($i, -1);
	$numArray = str_split($i);
	$numArray = array_sum($numArray);

	switch($t){
	
		case 1: //Visa
			if($lastNum%2 == 0){
				//even number
				//compare array to number
				if($numArray == 57){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				//odd number
				if($numArray == 31){
					return true;
				}
				else{
					return false;
				}
			}
			break;

		case 2: //MC
			if($lastNum%2 == 0){
				//even number
				if($numArray == 43){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				//odd number
				if($numArray == 24){
					return true;
				}
				else{
					return false;
				}
			}
			break;

		case 3: //Discover
			if($lastNum%2 == 0){
				//even number
				if($numArray == 52){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				//odd number
				if($numArray == 27){
					return true;
				}
				else{
					return false;
				}
			}
			break;
	}
	
	
	
}


$ccValid = ccType($firstNum);

if ($ccValid == true){
	$ccValid = matchType($firstNum, $type);
	
	if($ccValid == true){
		$ccValid = validate($cc, $type);
		if($ccValid == true){
			//continue to thank you page
			header('Location: http://cnn.com/');
			exit();
			//always put an exit after a header
		}
		else{
			echo'Invalid Credit Card Number';
		}
	}
	else{
		echo 'Credit Card Does Not Match Type';
	}
}
else {
	echo 'Credit Card is Invalid';
}


//Visa: if last digit is odd: sum of numbers = 31, if even: sum of numbers = 57

//MC: if last digit is odd: sum of numbers =24, if even: sum of numbers = 43

//Discover: if last digit is odd: sum of numbers =27, if even: sum of numbers = 52

//If validation fails, show error message, otherwise continue onto a thank you page (header: location)
